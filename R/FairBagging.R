#' FairBagging: Construction of a fair estimator with post-processing bagging method
#'
#' @description
#' \code{FairBagging} function constructs a fair estimator with post-processing bagging method.
#' The idea is to find a optimal weight attribution (vector w) so that the estimator after bagging \strong{sum(wi*estimator_i)} is fair.
#' In this algorithm the loss function is defined as the weighted average loss for demographic parity(DP), equalized odds(EO) and lack of disparate mistreatment(LDM).
#' Then with a parameter \code{lambda}, the prediction loss (MSE) is also added. All the loss formula are shown as following:
#'
#' \strong{The DP loss} = |mean(Y_pred*omega|S=0) - mean(Y_pred*omega|S=1)|^alpha.
#'
#' For each y, \strong{r0(y)} = sum(Y_pred*omega*K(y_true_i-y) | S=0) / sum(K(y_true_i-y) | S=0)
#'
#' \strong{r1(y)} = sum(Y_pred*omega*K(y_true_i-y) | S=1) / sum(K(y_true_i-y) | S=1)
#'
#' \strong{the EO loss} = mean(|r0(y)-r1(y)|^a)
#'
#' \strong{The LDM loss} = |mean(|Y_pred*omega - y_true|^a | S=0) - mean((Y_pred*omega - y_true)^a | S=1)|^b
#'
#' And \strong{the prediction loss} = MSE(Y_pred*omega, y_true)
#'
#' \strong{The final loss = mu0 * DP loss + mu1 * EO loss + mu2 * LDM loss + lambda * the prediction loss}
#'
#' @param Y_pred a matrix of predicted value, each column represents the prediction of one estimator
#' @param y_true real value of the target vector
#' @param X_test value of covariates used for prediction
#' @param colname_S the column name of the sensible variable
#' @param lambda the tuning parameter between perfect fairness and highest prediction power.
#' When \code{lambda}=0, the we target the perfect fairness, and when \code{lambda}>>1, then we target at choosing the best predictor
#' @param alpha_DP the parameter used to calculate the DP loss
#' @param a_EO a parameter used to calculate the EO loss (see the formula above)
#' @param a_LDM a parameter used to calculate the LDM loss (see the formula above)
#' @param b_LDM another parameter used to calculate the LDM loss (see the formula above)
#' @param theta the power parameter for the prediction loss. When \code{theta}=2, then the prediction loss is MSE.
#' @param faircrit chosen between 'DP'(demographic parity), 'EO'(equalized odds), 'LDM'(lack of disparate mistreatment) et 'mix'
#' @param limit_w TRUE if we want to limit the value of every weight element wi in [0,1]. The default value is FALSE.
#' @param mu0 the percentage of DP loss in the total loss. This is only needed if \code{faircrit}='mix'.
#' @param mu1 the percentage of EO loss in the total loss. The percentage of LDM loss is calculated by 1-mu0-mu1.This is only needed if \code{faircrit}='mix'.
#' @param type 'classification' or 'regression'. The default value is 'classification'
#' @param fair_discr
#' \itemize{
#' \item \strong{'smooth'} the \code{Y_pred*omega} in the formula of fairness loss above will be replaced
#' by a smooth function \code{f(Y_pred*omega)} for discretisation purpose. (Only available for classfiaction use).
#' The smooth function here is the cdf of a Gaussian distribution.
#' \item \strong{'discret'} the \code{Y_pred*omega} in the formula of fairness loss above will be replaced
#' by a stair function \code{1(Y_pred*omega>0.5)} for discretisation purpose. (Only available for classfiaction use)
#' \item \strong{'entropy'} We use the KL distance to calculate the precision loss.
#' \strong{The DP loss}=p1*log(p1/p0)+(1-p1)*log((1-p1)/(1-p0)) with p0=mean(f(Y_pred*omega)|S=0) and p1=mean(f(Y_pred*omega)|S=1).
#' }
#' @param prec_discr
#' \itemize{
#' \item \strong{'smooth'} the \code{Y_pred*omega} in the formula of precision loss above will be replaced
#' by a smooth function \code{f(Y_pred*omega)} for discretisation purpose. (Only available for classfiaction use).
#' The smooth function here is the cdf of a Gaussian distribution.
#' \item \strong{'discret'} the \code{Y_pred*omega} in the formula of precision loss above will be replaced
#' by a stair function \code{1(Y_pred*omega>0.5)} for discretisation purpose. (Only available for classfiaction use)
#' \item \strong{'entropy'} We use the cross entropy to calculate the precision loss.
#' \strong{the prediction loss}: \strong{the prediction loss}=-f(Y_pred*omega)*y_true-(1-f(Y_pred*omega))*(1-y_true), with f the smooth function.
#' }
#' @param brute_force if \code{TRUE} we will use the grid search method for optimization.
#' \code{fair_discr} and \code{prec_discr} are both 'discret' in this case. In this case, the optimisation could be inaccurate.
#'
#' @param sigma smoothing parameter for discretisation.
#'
#' @return \strong{weights} the optimal weight vector found (with size (M,1), M is the number of basic estimators)
#' @return \strong{y_pred} the prediction after bagging
#' @export
#' @references Fair learning with bagging

FairBagging <- function(Y_pred, y_true, X_test, colname_S,
                        lambda, alpha_DP = 2, a_EO = 2, a_LDM = 2, b_LDM = 1, theta = 2,
                        faircrit, limit_w = FALSE, mu0 = NaN, mu1 = NaN, type = "classification",
                        fair_discr = "smooth", prec_discr = "discret", brute_force = FALSE, sigma = 0.3) {
  # Sanity check
  faircrit <- match.arg(faircrit, c("DP", "EO", "LDM", "mix"))
  type <- match.arg(type, c("classification", "regression"))
  fair_discr <- match.arg(fair_discr, c("smooth", "discret", "entropy"))
  prec_discr <- match.arg(prec_discr, c("smooth", "discret", "entropy"))
  if ((faircrit == "mix") && (is.na(mu0) || is.na(mu1))) {
    errorCondition("when faircrit is mix, you need to specify the values of mu0 and mu1.")
  }
  if (KL_fairness && (faircrit != "DP")) {
    errorCondition("Sorry we have only provide the KL fairness option for Demographic Parity.")
  }

  # discretisation choices for fairness
  if (fair_discr == "smooth") {
    smooth <- TRUE
    KL_fairness <- FALSE
    discr_fair <- FALSE
  } else if (fair_discr == "discret") {
    smooth <- FALSE
    KL_fairness <- FALSE
    discr_fair <- FALSE
  } else if (fair_discr == "entropy") {
    smooth <- FALSE
    KL_fairness <- TRUE
    discr_fair <- FALSE
  }
  # discretisation choices for precision
  if (prec_discr == "smooth") {
    smooth <- TRUE
    discr_prec <- FALSE
    cross_entropy <- FALSE
  } else if (prec_discr == "discret") {
    discr_prec <- TRUE
    cross_entropy <- FALSE
  } else if (prec_discr == "entropy") {
    discr_prec <- FALSE
    cross_entropy <- TRUE
  }


  if (faircrit == "DP") {
    mu0 <- 1
    mu1 <- 0
  } else if (faircrit == "EO") {
    mu0 <- 0
    mu1 <- 1
  } else if (faircrit == "LDM") {
    mu0 <- 0
    mu1 <- 0
  }
  # initialization for omega
  M <- dim(Y_pred)[2] # number of estimators
  omega <- matrix(1 / M, M - 1, 1) # equal weights


  # Quick definite solution
  # if((faircrit=='DP')&&(alpha_DP==2)&&(!limit_w)&&(mu0==1)&&(mu1==0)&&(lambda>0)&&(!discr)){ #lambda !=0, regression or clasification but discr =FALSE
  #   # In the situation of DP, when alpha_DP=2 and
  #   # there is no limit for the value of omega, we calculate the omega_optim directly
  #   df0 <- cbind(Y_pred, y_true, X_test)
  #   delta_DP <- as.matrix(colMeans(df0[df0[[colname_S]]==0,])[1:M] - colMeans(df0[df0[[colname_S]]==1,])[1:M])
  #   Sigma <-  t(as.matrix(df0[,1:M]-df0[,'y_true'])) %*% as.matrix(df0[,1:M]-df0[,'y_true']) / (dim(Y_pred)[1])
  #   Sigma_DP <- delta_DP %*% t(delta_DP) + lambda * Sigma
  #   omega_optim <- as.matrix(rowSums(solve(Sigma_DP)) / sum(solve(Sigma_DP)))
  # }else if((faircrit=='EO')&&(a_EO==2)&&(!limit_w)&&(mu0==0)&&(mu1==1)&&(lambda>0)&&(!discr)){#lambda !=0, regression or clasification but discr =FALSE
  #   # In the situation of EO, when a_EO=2 and
  #   # there is no limit for the value of omega, we calculate the omega_optim directly
  #   df0 <- cbind(Y_pred, y_true, X_test)
  #   Sigma <-  t(as.matrix(df0[,1:M]-df0[,'y_true'])) %*% as.matrix(df0[,1:M]-df0[,'y_true']) / (dim(Y_pred)[1])
  #   #calculate bandwidth
  #   h <- sqrt(stats::var(rowMeans(Y_pred) - y_true))/(length(y_true)^0.2)
  #   r0_r1 <- function(y){ #sum(Y_pred*omega*K(y_true_i-y) | S=0) / sum(K(y_true_i-y) | S=0) - sum(Y_pred*omega*K(y_true_i-y)) / sum(K(y_true_i-y))
  #     #r0 = (K(y_true_i-y) | S=0) / sum(K(y_true_i-y) | S=0)
  #     r0 <- t(as.matrix(evmix::kdgaussian(df0[df0[[colname_S]]==0,]$y_true - y, lambda = h))) %*% as.matrix(df0[df0[[colname_S]]==0,][,1:M]) / sum(evmix::kdgaussian(df0[df0[[colname_S]]==0,]$y_true-y, lambda = h))
  #     # r = (Y_pred*K(y_true_i-y)) / sum(K(y_true_i-y))
  #     r1 <- t(as.matrix(evmix::kdgaussian(df0[df0[[colname_S]]==1,]$y_true - y, lambda = h))) %*% as.matrix(df0[df0[[colname_S]]==1,][,1:M]) / sum(evmix::kdgaussian(df0[df0[[colname_S]]==1,]$y_true-y, lambda = h))
  #     t(as.matrix(r0-r1)) %*% as.matrix(r0-r1)
  #   }
  #   Sigma_h <- matrix(rowMeans(apply(matrix(df0[['y_true']]),1,r0_r1)), nrow = M)
  #   Sigma_EO <- Sigma_h + lambda*Sigma
  #   omega_optim <- as.matrix(rowSums(solve(Sigma_EO)) / sum(solve(Sigma_EO)))
  # }else if((faircrit=='LDM')&&(a_LDM==2)&&(!limit_w)&&(b_LDM==1)&&(mu0==1)&&(mu1==0)&&(lambda>0)&&(!discr)){
  #   # In the situation of LDM, when a_LDM=2, b_LDM=1 and
  #   # there is no limit for the value of omega, we calculate the omega_optim directly
  #
  # }


  # first version of optimisation "Nelder-Mead"
  # set the constraints
  # if(limit_w){ #limit each wi>=0 and sum(wi)=1 (we can deduce that wi<=1)
  #   ui <- matrix( 1 , M+2 , M)
  #   ui[2,] <- -1
  #   ui[3:(M+2),] <- diag(M)
  #   ci <- c(0.99999 , -1.00001 , 0*(1:M) )
  #   res = stats::constrOptim( omega,
  #                             loss_mix(Y_pred, y_true, X_test, colname_S, mu0, mu1, lambda, alpha_DP, a_EO, a_LDM, b_LDM, theta, type, discr, sigma),
  #                             NULL,
  #                             ui,
  #                             ci,
  #                             method="Nelder-Mead",
  #                             outer.iterations = 100)
  # }else{ #no limit for the value of each wi, the only constraint is that sum(wi)=1
  #   # ui <- matrix( 1 , 2 , M)
  #   # ui[2,] <- -1
  #   # ci <- c(0.99999 , -1.00001)
  #   res = stats::optim( omega,
  #                       loss_mix(Y_pred, y_true, X_test, colname_S, mu0, mu1, lambda, alpha_DP, a_EO, a_LDM, b_LDM, theta, type, discr, sigma),
  #                       method="Nelder-Mead")
  # }

  # optimization
  # omega_optim <- c(res$par, 1-sum(res$par))

  if (brute_force) {
    if (limit_w) {
      sol <- NMOF::gridSearch(
        fun = loss_mix(Y_pred, y_true, X_test, colname_S, mu0, mu1, lambda, alpha_DP, a_EO, a_LDM, b_LDM, theta, type, smooth, sigma, brute_force, discr_prec, cross_entropy, KL_fairness),
        levels = list(seq(0, 1, by = 0.05), seq(0, 1, by = 0.05), seq(0, 1, by = 0.05), seq(0, 1, by = 0.05))
      )
    } else {
      sol <- NMOF::gridSearch(
        fun = loss_mix(Y_pred, y_true, X_test, colname_S, mu0, mu1, lambda, alpha_DP, a_EO, a_LDM, b_LDM, theta, type, smooth, sigma, brute_force, discr_prec, cross_entropy, KL_fairness),
        levels = list(seq(-0.5, 1.5, by = 0.1), seq(-0.5, 1.5, by = 0.1), seq(-0.5, 1.5, by = 0.1), seq(-0.5, 1.5, by = 0.1))
      )
    }
    omega_optim <- c(sol$minlevels, 1-sum(sol$minlevels))
  }else{
    if(limit_w){
      res0 <- nloptr::slsqp(x0 = omega,
                            fn = loss_mix(Y_pred, y_true, X_test, colname_S, mu0, mu1, lambda, alpha_DP, a_EO, a_LDM, b_LDM, theta, type, smooth, sigma, discr_direct, discr_prec, cross_entropy, KL_fairness),
                            lower = rep(0, N),
                            upper = rep(1, N))#,
      #heq = function(x) sum(x) - 1)
    }else{
      res0 <- nloptr::slsqp(x0 = omega,
                            fn = loss_mix(Y_pred, y_true, X_test, colname_S, mu0, mu1, lambda, alpha_DP, a_EO, a_LDM, b_LDM, theta, type, smooth, sigma, discr_direct, discr_prec, cross_entropy, KL_fairness),
                            control = list(xtol_rel = 1e-5, maxeval = 700, ftol_abs = 1e-5), nl.info=TRUE
                            )
      # res0 = stats::optim( omega,
      #                      loss_mix(Y_pred, y_true, X_test, colname_S, mu0, mu1, lambda, alpha_DP, a_EO, a_LDM, b_LDM, theta, type, smooth, sigma, discr_direct, discr_prec),
      #                     method="Nelder-Mead")
      #heq = function(x) sum(x) - 1)
    }
    omega_optim <- c(res0$par, 1 - sum(res0$par))
  }

  # result
  y_pred_new <- Y_pred %*% omega_optim

  return(list(
    weights = omega_optim,
    y_pred = y_pred_new
  ))
}
