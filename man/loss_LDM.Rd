% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/loss_functions.R
\name{loss_LDM}
\alias{loss_LDM}
\title{loss_LDM: the loss function of lack of disparate mistreatment (LDM)}
\usage{
loss_LDM(
  omega,
  Y_pred,
  y_true,
  X_test,
  colname_S,
  a = 2,
  b = 2,
  type = "classification",
  smooth = FALSE,
  sigma = 0.3,
  discr_fair = FALSE
)
}
\arguments{
\item{omega}{the weight vector, with size (M,1), where M is the number of estimators}

\item{Y_pred}{a matrix(n,M) of predicted value, each column represents the prediction of one estimator.
n is the number of observations and M is the number of estimators}

\item{y_true}{real value of the target vector}

\item{X_test}{value of covariates used for prediction}

\item{colname_S}{the column name of the sensible variable}

\item{a}{a parameter used to calculate the LDM loss (see the formula above)}

\item{b}{another parameter used to calculate the LDM loss (see the formula above)}

\item{type}{'classification' or 'regression'. The default value is 'classification'}

\item{smooth}{\code{type}='classification', \code{discr}=TRUE means the weighted average prediction will be discretized with a smooth function.}

\item{sigma}{smoothing parameter for discretisation}

\item{discr_fair}{\code{type}='classification', this leads to use stair function in the calculation of fairness loss.}
}
\value{
the loss of fairness penalized with the loss of predictive power (mean squared error)
}
\description{
\code{loss_LDM} function calculates the criterion with lack of disparate mistreatment (LDM) penalized with the loss of predictive power.
There are two parts of this loss, the LDM fairness loss and the prediction loss.

\strong{The LDM loss}: |mean(|Y_pred*omega - y_true|^a | S=0) - mean((Y_pred*omega - y_true)^a | S=1)|^b
}
\references{
Fair learning with bagging
}
