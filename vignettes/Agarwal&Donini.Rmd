---
title: "Agarwal & Donini"
author: "Xuwen Liu"
date: "`r Sys.Date()`"
output:
  html_document:
    toc: true
    toc_float: true
    df_print: paged
    number_sections: true
vignette: >
  %\VignetteIndexEntry{"Agarwal & Donini"}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r dependencies, message=FALSE, warning=FALSE}
library(FairBagging)
```

```{r}
df <- read.csv("../../data/simu_LDM_agarwal.csv")
df <- df[,-1]
df
```

```{r}
y.pred.logi <- df[['y_pred_agarwal_logi']]
#y.pred.knn <- df[['y_pred_hardt_knn']]
y.pred.svm <- df[['y_pred_agarwal_svm']]
y.pred.RF <- df[['y_pred_agarwal_RF']]
#y.pred.nn1 <- df[['y_pred_hardt_nn1']]
y_true <- df[['Y']]
X_test <- df[,!(names(df) %in% c('Y',
                                 #'y_pred_hardt_nn1',
                                 'y_pred_agarwal_RF',
                                 'y_pred_agarwal_svm',
                                 #'y_pred_hardt_knn',
                                 'y_pred_agarwal_logi'))]

colname_S <- 'S'
type <- 'classification'
alpha <- 1
beta <- 1
gamma <- 2

```


```{r}
df_fair_eval <- data.frame(matrix(rep(0, 5), nrow = 1))
colnames(df_fair_eval) <- c('estimator','DI_alpha','MLF_beta','DMR_gamma', 'RMSE')
res <- fairness_eval(y.pred.logi, y_true, X_test, colname_S, type, alpha, beta, gamma)
df_fair_eval[1,] <- c('logistic', res$DI_alpha, res$MLF_beta, res$DMR_gamma, 
                      caret::postResample(y.pred.logi, y_true)[['RMSE']])
res <- fairness_eval(y.pred.knn, y_true, X_test, colname_S, type, alpha, beta, gamma)
df_fair_eval[2,] <- c('kNN', res$DI_alpha, res$MLF_beta, res$DMR_gamma, 
                       caret::postResample(y.pred.knn, y_true)[['RMSE']])
res <- fairness_eval(y.pred.svm, y_true, X_test, colname_S, type, alpha, beta, gamma)
df_fair_eval[3,] <- c('SVM', res$DI_alpha, res$MLF_beta, res$DMR_gamma, 
                      caret::postResample(y.pred.svm, y_true)[['RMSE']])
res <- fairness_eval(y.pred.RF, y_true, X_test, colname_S, type, alpha, beta, gamma)
df_fair_eval[4,] <- c('Random Forest', res$DI_alpha, res$MLF_beta, res$DMR_gamma, 
                      caret::postResample(y.pred.RF, y_true)[['RMSE']])
res <- fairness_eval(y.pred.nn1, y_true, X_test, colname_S, type, alpha, beta, gamma)
df_fair_eval[5,] <- c('Neural Network', res$DI_alpha, res$MLF_beta, res$DMR_gamma,
                      caret::postResample(y.pred.nn1, y_true)[['RMSE']])
df_fair_eval
```

```{r}
df_fair_eval <- data.frame(matrix(rep(0, 6), nrow = 1))
colnames(df_fair_eval) <- c('estimator','DI_alpha','MLF_beta','DMR_gamma', 'precision', 'RMSE')
res <- fairness_eval(y.pred.logi, y_true, X_test, colname_S, type, alpha, beta, gamma)
df_fair_eval[1,] <- c('linear', res$DI_alpha, res$MLF_beta, res$DMR_gamma, 
                      caret::precision(factor(y.pred.logi), factor(y_true)),
                      caret::postResample(y.pred.logi, y_true)[['RMSE']])
# res <- fairness_eval(y.pred.knn, y_true, X_test, colname_S, type, alpha, beta, gamma)
# df_fair_eval[2,] <- c('kNN', res$DI_alpha, res$MLF_beta, res$DMR_gamma, 
#                       caret::precision(factor(y.pred.knn), factor(y_true)), 
#                       caret::postResample(y.pred.knn, y_true)[['RMSE']])
res <- fairness_eval(y.pred.svm, y_true, X_test, colname_S, type, alpha, beta, gamma)
df_fair_eval[2,] <- c('SVM', res$DI_alpha, res$MLF_beta, res$DMR_gamma, 
                      caret::precision(factor(y.pred.svm), factor(y_true)), 
                      caret::postResample(y.pred.svm, y_true)[['RMSE']])
res <- fairness_eval(y.pred.RF, y_true, X_test, colname_S, type, alpha, beta, gamma)
df_fair_eval[3,] <- c('Random Forest', res$DI_alpha, res$MLF_beta, res$DMR_gamma, 
                      caret::precision(factor(y.pred.RF), factor(y_true)), 
                      caret::postResample(y.pred.RF, y_true)[['RMSE']])
# res <- fairness_eval(y.pred.nn1, y_true, X_test, colname_S, type, alpha, beta, gamma)
# df_fair_eval[5,] <- c('Neural Network', res$DI_alpha, res$MLF_beta, res$DMR_gamma, 
#                       caret::precision(factor(y.pred.nn1), factor(y_true)), 
#                       caret::postResample(y.pred.nn1, y_true)[['RMSE']])
df_fair_eval
```

```{r}
write.csv(df_fair_eval, 'simu_LDM_agarwal.csv')
```

